#!/bin/bash
source globals.sh
source ${PROPERTY_FILE}
sudo scp -i $PEMDesginationFolder ${HostUserName}@${HostIPAddress}:${DownloadFileLocation} $currentfolder
echo -e "#### \e[0;34m Downloaded latest docker-compose.yml from server for Updation \e[0m"
sudo chmod 777 $infile
sudo chmod 777 $outfile
Var1=""O
IFS=''
while read -r line1
   do  
       Val21="$(cut -d':' -f2 <<<"$line1")"
       Val11=$(echo $Val21 | tr -d ' ')
       Var2=""O
       while read line
         do
           Var2=""I
           textvalue=$line
           Val1="$(cut -d':' -f1 <<<"$line")"
             if [[ "$Val1" == "$Val11" ]]; then
              echo -e "#### \e[0;34m Image Updated in Docker-compose File " $textvalue. "\e[0m"
              line1="    image: "$textvalue
             fi
       done < "$newImagelocal"
   echo $line1 >> "$outfile"
done < "$infile"
sudo chmod 777 $outfile
mv $infile $renameinfile
mv $outfile $renameoutfile
if [[ "$Var1" == "$Var2" ]]; then
    echo -e "#### \e[1;31m No new Images are avaiable for the Updation.\e[0m"
 else
    sudo scp -i $PEMDesginationFolder $renameoutfile ${HostUserName}@${HostIPAddress}:${UploadFolderlocation}
    #echo -e "#### \e[1;31m New docker-compose file Updated on server.\e[0m"
fi
echo -e "#### \e[1;31m Image Deployment - Done.\e[0m"
