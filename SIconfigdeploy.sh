#!/bin/bash
source globals.sh
source ${PROPERTY_FILE}
Var1=""O
Var4=""O
input=$sigmaIndia/Sigmaindiaconfig.txt
sudo chmod +x $input
while IFS='' read -r line || [[ -n "$line" ]];
    do
       Var4=""I
       Val21="$(cut -d'/' -f2 <<<"$line")"
       Val11=$(echo $Val21 | tr -d ' ')
       
         if [[ "Templates" == "$Val11" ]]; then
               echo -e "###############################################"
               gulp --configuration qa publishTemplate --file ${product_artifact}/$line
               echo -e "#### \e[0;34m Template Updated for $line. \e[0m"
               echo -e "###############################################"
         fi
         
         if [[ "Configuration" == "$Val11" ]]; then
               echo -e "###############################################"
               gulp --configuration qa publishConfiguration --file ${product_artifact}/$line
               echo -e "#### \e[0;34m Configuration Updated for $line. \e[0m"
               echo -e "###############################################"
         fi
         
         if [[ "Rules" == "$Val11" ]]; then
               echo -e "###############################################"
               gulp --configuration qa publishRule --file ${product_artifact}/$line
               echo -e "#### \e[0;34m Rule Updated for $line. \e[0m"
               echo -e "###############################################"
         fi
     
done <"$input"
if [[ "$Var1" == "$Var4" ]]; then
    echo -e "#### \e[1;31m No New Configuration/Template change made for sigma-india Tenant \e[0m"
 else
    echo -e "#### \e[1;31m New Configuration/Template change for Sigma-india tenant are published\e[0m"
fi
echo -e "#### \e[1;31m Configuration/Template deployment for Sigma-India Tenant - Done.\e[0m"
