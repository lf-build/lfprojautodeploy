# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary : Repository for  Auto-Deployment
* Version : 1.0.0

### How do I get set up? ###

1. Install npm
2. Install gulp from npm
3. Install gulp-replace
4. Install yargs
5. Install unirest
6. Install bluebird
7. Install harp-minify

### How do I Execute? ###

1. Update Image details which are needs to deployed in newImage.txt
2. Update Config file details for Sigma-India in Sigmaindiaconfig.txt
3. Update config file details for Sigma in Sigmaconfig.txt
4. Execute deployment.sh to Update new image on docker-compose.yml
6. Execute SIconfigdeploy.sh to post configuration of Sigma-India
7. Execute Sconfigdeploy.sh to post configuration of Sigma
8. And At Last to restart all services - Execute serviceRestart.sh

### Author and point of contact : RENGARAJAN M (rengarajan.m@lendfoundry.com)


