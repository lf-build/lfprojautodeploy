#!/bin/bash
source globals.sh
source ${PROPERTY_FILE}
sudo chmod 777 $AutoConfigdeployment -R
sudo chmod 777 $sigma -R
sudo chmod 777 $sigmaIndia -R
sudo chmod 777 $vscodetask -R
cd $AutoConfigdeployment
echo -e "#### \e[1;31m Image Deployment - Started.\e[0m" >> $AutoConfigdeployment/${FolderName}/deployment.log
./Imagedeployment.sh >> $AutoConfigdeployment/${FolderName}/deployment.log
./movReq.sh >> $AutoConfigdeployment/${FolderName}/deployment.log
cd $sigma
npm install gulp
npm install gulp-replace
npm install yargs
npm install unirest
npm install bluebird
npm install harp-minify
echo -e "#### \e[1;31m Configuration/Template deployment for Sigma Tenant - Started\e[0m" >> $AutoConfigdeployment/${FolderName}/deployment.log
./Sconfigdeploy.sh >> $AutoConfigdeployment/${FolderName}/deployment.log
#rm $sigma/Sigmaconfig.txt
cd $sigmaIndia
npm install gulp
npm install gulp-replace
npm install yargs
npm install unirest
npm install bluebird
npm install harp-minify
echo -e "#### \e[1;31m Configuration/Template deployment for Sigma-India Tenant - Started\e[0m" >> $AutoConfigdeployment/${FolderName}/deployment.log
./SIconfigdeploy.sh >> $AutoConfigdeployment/${FolderName}/deployment.log
rm $sigmaIndia/Sigmaindiaconfig.txt
echo -e "#### \e[1;31m Services ReStart In-Order- Started.\e[0m" >> $AutoConfigdeployment/${FolderName}/deployment.log
cd $AutoConfigdeployment
./serviceRestart.sh >> $AutoConfigdeployment/${FolderName}/deployment.log
echo -e "#### \e[1;31m Deployment-Completed.\e[0m" >> $AutoConfigdeployment/${FolderName}/deployment.log
