#!/bin/bash
source globals.sh
source ${PROPERTY_FILE}
Var1=""O
Var4=""O
input=$sigma/Sigmaconfig.txt
sudo chmod +x $input
while IFS='' read -r line3 || [[ -n "$line3" ]];
   do
       Var4=""I
       Val31="$(cut -d'/' -f2 <<<"$line3")"
       Val51=$(echo $Val31 | tr -d ' ')
       
         if [[ "Templates" == "$Val51" ]]; then
               echo -e "###############################################"
               gulp --configuration qa publishTemplate --file ${product_artifact}/$line3
               echo -e "#### \e[0;34m Template Updated for $line3. \e[0m"
               echo -e "###############################################"
         fi
         
         if [[ "Configuration" == "$Val51" ]]; then
               echo -e "###############################################"
               gulp --configuration qa publishConfiguration --file ${product_artifact}/$line3
               echo -e "#### \e[0;34m Configuration Updated for $line3. \e[0m"
               echo -e "###############################################"
         fi
         
         if [[ "Rules" == "$Val51" ]]; then
               echo -e "###############################################"
               gulp --configuration qa publishRule --file ${product_artifact}/$line3
               echo -e "#### \e[0;34m Rule Updated for $line3. \e[0m"
               echo -e "###############################################"
         fi
         
done <"$input"
if [[ "$Var1" == "$Var4" ]]; then
    echo -e "#### \e[1;31m No New Configuration/Template change made for sigma Tenant \e[0m"
 else
    echo -e "#### \e[1;31m New Configuration/Template change for Sigma tenant are published\e[0m"
fi
echo -e "#### \e[1;31m Configuration/Template deployment for Sigma Tenant - Done.\e[0m"